package service;

import io.grpc.stub.StreamObserver;
import proto.Zodiac;
import proto.ZodiacServiceGrpc;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ZodiacClass extends ZodiacServiceGrpc.ZodiacServiceImplBase {

    @Override
    public void getDate(Zodiac.DateRequest request, StreamObserver<Zodiac.SignResponse> responseObserver) {

        Zodiac.SignResponse.Builder response = Zodiac.SignResponse.newBuilder();

        String sign = FindSign(request.getBirthDate());
        response.setSign(sign);

        responseObserver.onNext(response.build());
        responseObserver.onCompleted();
    }

    public String FindSign(String date)
    {
        String sign = "";
        File file = new File("D:\\A.FACULTATE\\an II\\CNA\\Tema2.3\\Tema2.3_Server\\src\\main\\resources\\zodiacIntervals");
        Scanner scanner = null;
        try {
            scanner = new Scanner(file);
        } catch (FileNotFoundException e) {
            System.out.println("The file can't be open");
            System.exit(5);
        }

        String[] dateArray = date.split("/");

        while(scanner.hasNextLine())
        {
            String line = scanner.nextLine();

            String[] lineArray = line.split("-");
            String[] start = lineArray[0].split("/");
            String[] end = lineArray[1].split("/");

            int monthStart = Integer.parseInt(start[0]);
            int dayStart = Integer.parseInt(start[1]);

            int monthEnd = Integer.parseInt(end[0]);
            int dayEnd = Integer.parseInt(end[1]);

            if(Integer.parseInt(dateArray[0]) == monthStart)
            {
                if(Integer.parseInt(dateArray[1]) >= dayStart)
                {
                    sign = lineArray[2];
                }
            }
            else
            if(Integer.parseInt(dateArray[0]) == monthEnd)
            {
                if(Integer.parseInt(dateArray[1]) <= dayEnd)
                {
                    sign = lineArray[2];
                }
            }
        }

        return sign;
    }
}
