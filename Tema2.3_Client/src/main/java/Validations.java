public class Validations {

    public static boolean ValidInput(String date)
    {
        if(ValidFormat(date) && ValidContent(date) && ValidLenght(date))
        {
            return true;
        }

        return false;
    }

    public static boolean ValidFormat(String date)
    {
        String[] array = date.split("/");

        if(ValidMonth(array[0]) && ValidDay(array[1], array[0], array[2]))
        {
            return true;
        }

        return false;
    }

    public static boolean ValidContent(String date)
    {
        String regex = "[0-9 -/]+";
        if(date.matches(regex) && ((date.charAt(2) == '/' && date.charAt(5) == '/') || (date.charAt(1) == '/' && date.charAt(3) == '/')))
        {
            return true;
        }
        return false;
    }

    public static boolean ValidLenght(String date)
    {
        if(date.length() == 10 || date.length() == 8)
            return true;

        return false;
    }

    public static boolean ValidMonth(String str)
    {
        int month = Integer.parseInt(str);

        if(month > 0 && month < 13)
        {
            return true;
        }

        return false;
    }

    public static boolean LeapYear(String str)
    {
        int year = Integer.parseInt(str);

        if(year % 4 == 0 && year % 100 != 0)
        {
            return true;
        }

        if(year % 400 == 0)
        {
            return true;
        }

        return false;
    }

    public static boolean ValidDay(String day, String month, String year)
    {
        int dayInt = Integer.parseInt(day);
        int monthInt = Integer.parseInt(month);

        if((monthInt == 1 || monthInt == 3 || monthInt == 5 || monthInt == 7 || monthInt == 8 || monthInt == 10 || monthInt == 12) && dayInt <= 31)
        {
            return true;
        }

        if((monthInt == 4 || monthInt == 6 || monthInt == 9 || monthInt == 11) && dayInt <= 30)
        {
            return true;
        }

        if(LeapYear(year) && dayInt <= 29)
        {
            return true;
        }

        if(!LeapYear(year) && dayInt <= 28)
        {
            return true;
        }

        return false;
    }
}
