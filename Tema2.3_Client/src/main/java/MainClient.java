import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;
import proto.Zodiac;
import proto.ZodiacServiceGrpc;

import java.util.Scanner;

public class MainClient {

    public static void main(String[] args){

        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 8999).usePlaintext().build();

        ZodiacServiceGrpc.ZodiacServiceStub dataStub = ZodiacServiceGrpc.newStub(channel);

        System.out.println("What would you like to do?");
        System.out.println("1: Introduce birthdate");
        System.out.println("2: Disconnect");

        boolean isConnected = true;

        while(isConnected)
        {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Choose: ");
            int option = scanner.nextInt();

            switch (option){

                case 1: {
                    boolean valid = false;
                    while(!valid)
                    {
                        Scanner read = new Scanner(System.in);
                        System.out.println("Birthdate: ");
                        String birthdate = read.next();

                        if(!Validations.ValidInput(birthdate))
                        {
                            System.out.println("The date is not valid!");
                            System.out.println("Please introduce another date");
                        }

                        else
                        {
                            valid = true;
                            System.out.println("The date is correct!");

                            dataStub.getDate(Zodiac.DateRequest.newBuilder().setBirthDate(birthdate).build(), new StreamObserver<Zodiac.SignResponse>() {
                                @Override
                                public void onNext(Zodiac.SignResponse value) {
                                    System.out.println(value);
                                }

                                @Override
                                public void onError(Throwable t) {
                                    System.out.println("Error : " + t.getMessage());
                                }

                                @Override
                                public void onCompleted() {}
                            });
                        }
                    }
                    break;
                }

                case 2: {
                    isConnected = false;
                    break;
                }
                default:
                    System.out.println("Unknown command, insert a valid command!");
                    break;
            }
        }
    }
}
